import React, {Component} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
var parseString = require('xml2js').parseString;

const dataSourceConfig = {
    text: 'Description',
    value: 'ProviderID',
};

const styles = {
    customWidth: {
        width: '100%',
        textAlign: 'left'
    },
  };

export default class Routes extends Component {

    constructor() {
        super();
        this.state = { 
            dataSource: [],
            route: null 
        };
    }

    handleUpdateInput = (value) => {
        this.setState({
            route: value.Route
        });
        this.props.reportRoute(value.Route);
    }

    componentDidMount() {
        fetch('https://svc.metrotransit.org/NexTrip/Routes')
        .then(results => results.text()).then(data => {
            var json = [];
            parseString(data, function (err, result) {
                json = JSON.stringify(result);
            });
            if (JSON.parse(json).ArrayOfNexTripRoute.NexTripRoute) {
                JSON.parse(json).ArrayOfNexTripRoute.NexTripRoute.map(row => {
                    this.state.dataSource.push({  
                        Description: row.Description[0], 
                        ProviderID: row.ProviderID[0], 
                        Route: row.Route[0] 
                    });
                    return true;
                });
            } else {
                return false;
            }
        });      
    }

  render() {
    return (
      <div>
        <AutoComplete
            floatingLabelText="Enter a Route"
            filter={AutoComplete.fuzzyFilter}
            dataSource={this.state.dataSource}
            dataSourceConfig={dataSourceConfig}
            fullWidth={true}
            style={styles.customWidth}
            onNewRequest={this.handleUpdateInput}
            openOnFocus={true}
            maxSearchResults={5}
        />
      </div>
    );
  }
}