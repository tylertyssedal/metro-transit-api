import React, {Component} from 'react';
import Routes from './Routes';
import Directions from './Directions';
import Stops from './Stops';
import Results from './Results';

export default class SearchContainer extends Component {

    constructor() {
        super();
        this.state = { 
            route: null,
            direction: null,
            stop: null,
            directionsVisible: false,
            stopsVisible: false, 
            resultsVisible: false,
            clear: false
        };
    }

    setRoute = (value) => {
        this.setState({
            route: value,
            directionsVisible: true
        });
    }

    setDirection = (value) => {
        this.setState({
            direction: value,
            stopsVisible: true
        });
    }

    setStop = (value) => {
        this.setState({
            stop: value,
            resultsVisible: true,
            clear: true
        });
    }

    clearResults = (value) => {
        if (this.state.clear) { 
            document.body.classList.toggle('success');
            this.setState({
                route: null,
                direction: null,
                stop: null,
                directionsVisible: false,
                stopsVisible: false, 
                resultsVisible: false,
                clear: false
            });
        }
    }

    render() {
        return (
            <div>
                <div onClick={this.clearResults} >
                    <Routes reportRoute={this.setRoute}></Routes>
                    {this.state.directionsVisible ? <Directions route={this.state.route} reportDirection={this.setDirection}></Directions> : ""}
                    {this.state.stopsVisible ? <Stops route={this.state.route} direction={this.state.direction} reportStop={this.setStop}></Stops> : ""}
                </div>
                {this.state.resultsVisible ? <Results route={this.state.route} direction={this.state.direction} stop={this.state.stop} ></Results> : ""}
            </div>
        );
    }
}