import React, {Component} from 'react';
var parseString = require('xml2js').parseString;

export default class Results extends Component {

    constructor() {
        super();
        this.state = { 
            dataSource: []
        };
    }

    componentDidMount() {
        fetch('https://svc.metrotransit.org/NexTrip/' + this.props.route + '/' + this.props.direction + '/' + this.props.stop)
        .then(results => results.text()).then(data => {
            document.body.classList.toggle('success');
            var json = [];
            parseString(data, function (err, result) {
              json = JSON.stringify(result);
            });
            if (JSON.parse(json).ArrayOfNexTripDeparture.NexTripDeparture) {
                JSON.parse(json).ArrayOfNexTripDeparture.NexTripDeparture.map(row => {
                    this.state.dataSource.push(row.DepartureText[0]);
                    this.forceUpdate();
                    return true;
                });
            } else {
                return false;
            }
        });         
    }

  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <h2>Upcoming Stop Times</h2>
        <ul className="stop-times">
        {
            this.state.dataSource.map(function(time, index) {
                var best = (index === 0) ? 'best-time' : ''; 
                return <li key={index} className={ best }>{time}</li>
            })
        }
        </ul>
      </div>
    );
  }
}