import React, {Component} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
var parseString = require('xml2js').parseString;

const styles = {
  customWidth: {
    width: '100%',
    textAlign: 'left'
  },
};

const dataSourceConfig = {
  text: 'Name',
  value: 'Id',
};

export default class Directions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      direction: null
    };
  }

  handleUpdateInput = (value) => {
    this.setState({
      direction: value.Id
    });
    this.props.reportDirection(value.Id);
  }
  
  componentDidMount() {
    fetch('https://svc.metrotransit.org/NexTrip/Directions/' + this.props.route)
      .then(results => results.text()).then(data => {
          var json = [];
          parseString(data, function (err, result) {
            json = JSON.stringify(result);
          });
          if (JSON.parse(json).ArrayOfTextValuePair.TextValuePair) {
            JSON.parse(json).ArrayOfTextValuePair.TextValuePair.map(row => {
              this.state.dataSource.push({  
                Id: row.Value[0], 
                Name: row.Text[0] 
              });
              this.forceUpdate();
              return true;
            });
          } else {
            return false;
          }
    });      
  }

  render() {
    return (
      <div>
        <AutoComplete
            floatingLabelText="Select a Direction"
            filter={AutoComplete.fuzzyFilter}
            dataSource={this.state.dataSource}
            dataSourceConfig={dataSourceConfig}
            fullWidth={true}
            style={styles.customWidth}
            onNewRequest={this.handleUpdateInput}
            openOnFocus={true}
        />
      </div>
    );
  }
}