import React, {Component} from 'react';
import AutoComplete from 'material-ui/AutoComplete';
var parseString = require('xml2js').parseString;

const styles = {
  customWidth: {
    width: '100%',
    textAlign: 'left'
  },
};

const dataSourceConfig = {
  text: 'Name',
  value: 'Id',
};

export default class Stops extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      stop: null
    };
  }

  handleUpdateInput = (value) => {
    this.setState({
      stop: value.Id
    });
    this.props.reportStop(value.Id);
  }
  
  componentDidMount() {
    fetch('https://svc.metrotransit.org/NexTrip/Stops/' + this.props.route + '/' + this.props.direction)
      .then(results => results.text()).then(data => {
          var json = [];
          parseString(data, function (err, result) {
            json = JSON.stringify(result);
          });
          if (JSON.parse(json).ArrayOfTextValuePair.TextValuePair) {
            JSON.parse(json).ArrayOfTextValuePair.TextValuePair.map(row => {
              this.state.dataSource.push({  Id: row.Value[0], Name: row.Text[0] });
              this.forceUpdate();
              return true;
            });
          } else {
            return false;
          }
    });      
  }

  render() {
    return (
      <div>
        <AutoComplete
            floatingLabelText="Select a Stop"
            filter={AutoComplete.fuzzyFilter}
            dataSource={this.state.dataSource}
            dataSourceConfig={dataSourceConfig}
            fullWidth={true}
            style={styles.customWidth}
            onNewRequest={this.handleUpdateInput}
            openOnFocus={true}
        />
      </div>
    );
  }
}