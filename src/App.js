import React, { Component } from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import {deepOrange500} from 'material-ui/styles/colors';

import SearchContainer from './SearchContainer';
import Paper from 'material-ui/Paper';

const styles = {
  container: {
    textAlign: 'left',
    width: 600,
    paddingTop: 200,
    margin: 'auto'
  },
  paper: {
    padding: 20
  }
};

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      routes: []
    }
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={styles.container}>
          <Paper style={styles.paper} zDepth={1}>
            <div className="text-center">
             <img src="MetroTransitLogo.svg" alt="metro-transit-logo" className="logo"/>
            </div>
            <SearchContainer />
          </Paper>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
