#Metro Transit API 

##API consumption with React and Material UI.

1. Run `npm install`
2. Run `npm start`
3. Enjoy!

## Deploy with Terraform to S3

* Install Terraform: https://www.terraform.io/
* Install AWS CLI: https://aws.amazon.com/cli/

Configure s3.tf with your own bucket information. Also ensure you have your AWS credentially locally set up for CLI. 

* `terraform init`
* `terraform apply`
* `npm run build`
* `aws s3 sync build s3://metro-transit-api`

**Demo**: https://s3.amazonaws.com/metro-transit-api/index.html

